package org.kcsquared.marysue;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class CreditsState extends BasicGameState {
	
	private String[] credits = new String[3];
	
private static int stateID = -1;
	
	private int playerChoice = 0;
	private static final int BACK = 0;
	
	private String[] back = {"Back"};
	private Font font;
	private TrueTypeFont playerOptionsTTF;
	private Color notChosen = new Color(153,204,255);
	
	Image background;

	@SuppressWarnings("static-access")
	public CreditsState(int stateID) {
		this.stateID = stateID;
	}
	
	@Override
	public void init(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		
		font = new Font("Verdana", Font.BOLD, 40);
		playerOptionsTTF = new TrueTypeFont(font, true);
		font = new Font ("Verdana", Font.PLAIN, 20);
		background = new Image("data/backgrounds/EverfreeLowRez.png");
		
		credits[0] = "Creators: K.C. Squared";
		credits[1] = "Programming: Casey";
		credits[2] = "Art, Music, Map Design: KC";
		
	
		
	}

	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics arg2) throws SlickException {
		
		background.draw(0,0,1280,720); 
		renderWords();
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		
		Input input = gc.getInput();
		if (input.isKeyPressed(Input.KEY_ENTER) || input.isButton1Pressed(Input.ANY_CONTROLLER)) {
			switch (playerChoice) {
			case BACK : Main.game.enterState(Main.MENU); break;
			}
		}
		
	}
	
	public void renderWords() {
		
			 	
		
		for (int i = 0; i < credits.length; i++) {
				playerOptionsTTF.drawString(100, i * 50 + 200, credits[i], notChosen);
		}
		playerOptionsTTF.drawString(100, 3 * 50 + 200, back[0]);
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return stateID;
	}

}
