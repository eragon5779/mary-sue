package org.kcsquared.marysue;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;


public class Main extends StateBasedGame{
	
	public static final int MENU = 1;
	public static final int PLAY = 5;
	public static final int CREDITS = 3;
	public static final int OPTIONS = 4;
	public static final int PAUSE = 2;
	public static final int SPLASH = 0;
	
	public static Main game;
	AppGameContainer app;
	
	static boolean vsync = true;
	static boolean fps = false;
	
	public Main() throws SlickException {
		
		super("MarySue");
		//this.addState(new SplashState(SPLASH));
		
	}

	private void splashinit() throws SlickException {
		
		this.addState(new SplashState(SPLASH));
		try {
			this.getState(SPLASH).init(this.getContainer(), this);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		
		this.enterState(SPLASH);
		
		//loadStates();
	}
	
	public void loadStates() throws SlickException {
		
		this.addState(new MainMenuState(MENU));
		this.addState(new MarySue(PLAY));
		this.addState(new CreditsState(CREDITS));
		this.addState(new OptionsState(OPTIONS));
		this.addState(new PauseState(PAUSE));
		
		init();

	}
	
	public void init() throws SlickException {
		
		this.getState(MENU).init(game.getContainer(), this);
		this.getState(CREDITS).init(game.getContainer(), this);
		this.getState(OPTIONS).init(game.getContainer(), this);
		this.getState(PLAY).init(game.getContainer(), this);
		this.getState(PAUSE).init(game.getContainer(), this);
		
		this.enterState(MENU);
		
	}

	public static void main(String[] args) throws SlickException {
		
		game = new Main();
		
		game.app = new AppGameContainer(game);
		
		game.app.setAlwaysRender(true);
		game.app.setMultiSample(1);
		//game.app.setVSync(vsync);
		game.app.setTargetFrameRate(60);
		game.app.setDisplayMode(1280, 720, false);
		game.app.setMusicOn(false);
		game.app.setShowFPS(fps);
		game.app.start();
		
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		
		splashinit();
		
	}
}
