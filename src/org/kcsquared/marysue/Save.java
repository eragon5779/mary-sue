package org.kcsquared.marysue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import org.newdawn.slick.geom.Vector2f;

public class Save {
	
	private Vector2f coordinates = new Vector2f(0, 0);
	
	public Save() {
		
	}
	
	public void saveToFile(Vector2f vector) {
		
		try {
            PrintWriter kirjoittaja = new PrintWriter(new File("saves/game.sav"));
            kirjoittaja.println(vector.x);
            kirjoittaja.println(vector.y);
            kirjoittaja.close();
        } catch (Exception e) {
            System.out.println("Error: could not save the file!");
        }
		
	}
	
	public Vector2f loadFromFile() {
		
		try {
            Scanner lukija = new Scanner(new File("saves/game.sav"));
            coordinates.x = Float.parseFloat(lukija.nextLine());
            coordinates.y = Float.parseFloat(lukija.nextLine());
        } catch (FileNotFoundException e) {
            System.out.println("Error: saved game does not exist!");
        } catch (NumberFormatException e) {
            System.out.println("Error: could not read the file!");
        }
        System.out.println("Load succesful!");
        return coordinates;
		
	}

}
