package org.kcsquared.marysue;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

import org.kcsquared.marysue.collision.*;

public class Duk {

	//Basic Variables
		private String name;
		private Dialogue dialogue;
		
		//Animations
		private SpriteSheet sprites;
		private Animation sprite, up, down, left, right;
		
		//Position and Movement
		protected Vector2f pos;
		private static final int SIZE = 32;
		private Collision col;
		private int counter = 0;
		private boolean countUp = true, countDown;
		private Rectangle box;
		
		public Duk(float x, float y, int width, int height, String name, Dialogue dialogue) throws SlickException {
			pos = new Vector2f(x, y);
			box = new Rectangle(x, y, width, height);
			
			this.dialogue = dialogue;
			
			this.name = name;
			this.sprites = new SpriteSheet("data/sprites/duk/" + this.name + ".png", 32, 32, 0);
		}
		
		public void init() {
			Image[] movementUp = new Image[4];
			Image[] movementDown = new Image[4];
			Image[] movementLeft = new Image[4];
			Image[] movementRight = new Image[4];
			int[] duration = new int[4];
			
			for (int i = 0; i < 4; i++) {
				duration[i] = 100;
				movementUp[i] = sprites.getSprite(i, 0);
				movementDown[i] = sprites.getSprite(i, 0);
				movementRight[i] = sprites.getSprite(i, 0);
				movementLeft[i] = sprites.getSprite(3-i, 1);
			}

			up = new Animation(movementUp, duration, false);
			down = new Animation(movementDown, duration, false);
			left = new Animation(movementLeft, duration, false);
			right = new Animation(movementRight, duration, false);
			
			sprite = right;
		}
		
		public void update(GameContainer gc, int mapWidth, int mapHeight, int delta) throws SlickException {
			if (counter <= 100 && countUp) {
				sprite = right;
				counter++;
				pos.x++;
				sprite.update(delta);
			}
			if (counter == 100) {
				countDown = true;
				countUp = false;
				counter--;
				pos.x--;
			}
			if (counter >= 0 && countDown) {
				sprite = left;
				counter--;
				pos.x--;
				sprite.update(delta);
			}
			if (counter == 0 && countDown) {
				countUp = true;
				countDown = false;
				counter++;
				pos.x++;
			}
			box.setBounds(pos.x, pos.y, box.getWidth(), box.getHeight());
		}
		
		public void render() {
			sprite.draw(pos.x, pos.y);
			
		}
	
		public Shape getBox() {
			return this.box;
		}
		
}
