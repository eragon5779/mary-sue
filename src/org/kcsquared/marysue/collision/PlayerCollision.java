package org.kcsquared.marysue.collision;

import java.util.ArrayList;
import org.kcsquared.marysue.*;

public class PlayerCollision {
	
	private ArrayList<NPC> npcs;
	private Player player;
	private final int SIZE = 32;
	
	private NPC currentNPC;
	
	public PlayerCollision(ArrayList<NPC> npcs, Player player) {
		this.npcs = npcs;
		this.player = player;
	}
	
	public boolean isBlocked() {
		for (int i = 0; i < npcs.size(); i++) {
			if (player.getBox().intersects(npcs.get(i).getBox())) {
				currentNPC = npcs.get(i);
				return true;
			}
		}
		return false;
	}
	
	public boolean blockedSideRight() {
		
		if (isBlocked()) {
			if (player.getPos().x < npcs.get(npcs.indexOf(currentNPC)).getPos().x) {
				return true;
				
			}
		}
		return false;
	}
	public boolean blockedSideLeft() {
		if (isBlocked()) {
			if (player.getPos().x > npcs.get(npcs.indexOf(currentNPC)).getPos().x) {
				return true;
			}
		}
		return false;
	}
	
	public boolean blockedSideTop() {
		if (isBlocked()) {
			if (player.getPos().y < npcs.get(npcs.indexOf(currentNPC)).getPos().y) {
				return true;
			}
		}
		return false;
	}
	
	public boolean blockedSideBottom() {
		if (isBlocked()) {
			if (player.getPos().y > npcs.get(npcs.indexOf(currentNPC)).getPos().y) {
				return true;
			}
		}
		return false;
	}
	
	public NPC getCurrent() {
		return this.currentNPC;
	}
	
}
