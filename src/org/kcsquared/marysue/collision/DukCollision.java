package org.kcsquared.marysue.collision;

import java.util.ArrayList;
import org.kcsquared.marysue.*;

public class DukCollision {
	
	private Duk duk;
	private Player player;
	private final int SIZE = 32;
	
	private Duk currentDuk;
	
	public DukCollision(Duk duk, Player player) {
		this.duk = duk;
		this.player = player;
	}
	
	public boolean isBlocked() {
		
			if (player.getBox().intersects(MarySue.getDuk98().getBox())) {
				return true;
			}
		return false;
	}
	
	
	
	public Duk getCurrent() {
		return this.currentDuk;
	}
	
}
