package org.kcsquared.marysue.collision;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;

public class Collision {

	public boolean[][] blocked;
	public final static int SIZE = 16;
	Image collisionMap;
	
	public Collision(Image collisionMap) {
		
		this.collisionMap = collisionMap;
		blocked = new boolean[collisionMap.getWidth()][collisionMap.getHeight()];
		
		for (int x = 0; x < collisionMap.getWidth(); x++) {
			for (int y = 0; y < collisionMap.getWidth(); y++) {
				Color pixelColor = collisionMap.getColor(x, y);
				Color collisionColor = new Color(0,0,0,0);
				if (!pixelColor.equals(collisionColor)) {
					blocked[x][y] = true;
				}
			}
		}
	}
	
	public boolean isBlocked(float x, float y) {
		return blocked[(int)x][(int)y];
	}
	
	public Image getMap() {
		return this.collisionMap;
	}
}
