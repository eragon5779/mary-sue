package org.kcsquared.marysue;

import data.*;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.GameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.state.transition.Transition;

public class CrossFadeTransition implements Transition {

	private GameState state1;
	private GameState state2;
	private FadeOutTransition fade;
	private Image image;
	
	public CrossFadeTransition(int fadeMillis) {
		
		fade = new FadeOutTransition(Color.black, fadeMillis);
		
	}
	
	@Override
	public void init(GameState state1, GameState state2) {
		
		this.state1 = state1;
		this.state2 = state2;
		try {
			image = new Image("data/transition/gamecap.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean isComplete() {
		
		return fade.isComplete();
		
	}

	@Override
	public void postRender(StateBasedGame game, GameContainer gc, Graphics g) throws SlickException {
		
		state1.render(gc, game, g);
		g.clearAlphaMap();
		g.setDrawMode(Graphics.MODE_ALPHA_MAP);
		fade.postRender(game, gc, g);
		g.setDrawMode(Graphics.MODE_ALPHA_BLEND);
		//state2.render(gc, game, g);
		image.draw();
		g.setDrawMode(Graphics.MODE_NORMAL);
		
	}

	@Override
	public void preRender(StateBasedGame arg0, GameContainer arg1, Graphics arg2) throws SlickException {
				
	}

	@Override
	public void update(StateBasedGame game, GameContainer gc, int delta) throws SlickException {
		
		fade.update(game, gc, delta);
		
	}

}
