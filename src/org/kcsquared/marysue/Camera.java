package org.kcsquared.marysue;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.tiled.TiledMap;

public class Camera {
	
	private static int transX;
	private static int transY;
	private int mapWidth, mapHeight;
	private static Rectangle viewPort;
	
	public Camera(TiledMap map, int mapWidth, int mapHeight) {
		
		transX = 0;
		transY = 0;
		viewPort = new Rectangle(0, 0, MarySue.WIDTH, MarySue.HEIGHT);
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		
	}
	
	public void translate(Graphics g, Player entity) {
		
		if (entity.getX() - MarySue.WIDTH/2 + 16 < 0) {
			transX = 0;
		}
		
		else if (entity.getX() + MarySue.WIDTH/2 + 16 > mapWidth) {
			transX = -mapWidth + MarySue.WIDTH;
		}
		
		else {
			transX = (int)-entity.getX() + MarySue.WIDTH/2 - 16;
		}
		
		if(entity.getY() - MarySue.HEIGHT/2 + 16 < 0) {
	        transY = 0;
		}
		
	    else if(entity.getY() + MarySue.HEIGHT/2 + 16 > mapHeight) {
	        transY = -mapHeight + MarySue.HEIGHT;
	    }
		
	    else {
	        transY = (int)-entity.getY() + MarySue.HEIGHT/2 - 16;
	    }
		
		g.translate(transX, transY);
		viewPort.setX(-transX);
		viewPort.setY(-transY);
		
	}
	
	public static float getX() {
		return transX;
	}
	public static float getY() {
		return transY;
	}

}
