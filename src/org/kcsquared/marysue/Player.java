package org.kcsquared.marysue;

import java.awt.Font;
import java.util.ArrayList;

import org.kcsquared.marysue.collision.*;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;

import net.java.games.input.Controller;
 
public class Player {
	
	//QUAK
	static Music quak;
	
	//Other stuff
	private String name;
	private boolean showDiag;
	private boolean ePressed;
	private boolean escPressed;
	private boolean qPressed;
	private boolean duk98ColBool;
	private boolean sprinting;
	private boolean LEFT, RIGHT, UP, DOWN;
	private Vector2f trans;
	
	//input
	private Input input;
	
	//Collision Map
	private static final int WIDTH = 53;
	private static final int HEIGHT = 49;
	private static final int SPACING = 11;
	private static float SPEED = 0.2f;
	
	private Collision col;
	private DukCollision duk98Col;
	private PlayerCollision entCol;
	public ArrayList<NPC> npcs = MarySue.npcs;
	
	//NPC chatting
	private TrueTypeFont dialogueFont;
	private Font font;
	private NPC currentNPC = null;
	
	//Variables
	protected Vector2f pos;
	protected Rectangle box;
	private int mapWidth, mapHeight;
	
	//Animation
	private SpriteSheet sprites = new SpriteSheet("data/sprites/player/player.png", WIDTH, HEIGHT, SPACING);
	private Animation sprite, up, down, left, right;
	private int totalSteps = sprites.getWidth() / (WIDTH + SPACING);
	
	public Player(float x, float y, int width, int height, int mapWidth, int mapHeight, String name) throws SlickException {
		
		quak = new Music("data/music/quak.ogg");
		
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		
		pos = new Vector2f(x, y);
		box = new Rectangle(x, y, width, height);
		
		this.name = name;
		
		font = new Font("Courier New", Font.PLAIN, 40);
		dialogueFont = new TrueTypeFont(font, true);
		
		input = Main.game.getContainer().getInput();
		
		col = new Collision(new Image("data/collision/FirstLevel.png"));
		duk98Col = new DukCollision(MarySue.getDuk98(), this);
		entCol = new PlayerCollision(MarySue.npcs, this);
		input.clearControlPressedRecord();
	}
	
	public void update(GameContainer gc, int mapWidth, int mapHeight, int delta) throws SlickException {
		
		currentNPC = entCol.getCurrent();
		
		trans = new Vector2f(0,0);
		
		
		if (input.isKeyDown(Input.KEY_W) && !showDiag /* || input.isControllerUp(Input.ANY_CONTROLLER) */) {
			
			sprite = up;
			DOWN = true;
			
			if (!col.isBlocked(pos.x + WIDTH, pos.y - delta * 0.2f) && !col.isBlocked(pos.x, pos.y - delta * 0.2f) && 
					!entCol.blockedSideBottom()) {
				
				sprite.update(delta);
				trans.y = -SPEED * delta;
				
			}
		}
		
		if (input.isKeyDown(Input.KEY_S) && !showDiag /*|| input.isControllerDown(Input.ANY_CONTROLLER)*/) {
			
			sprite = down;
			UP = true;
			
			if (!col.isBlocked(pos.x + WIDTH, pos.y + HEIGHT + delta * 0.2f) && !col.isBlocked(pos.x, pos.y + HEIGHT + delta * 0.2f) && 
					!entCol.blockedSideTop()) {
				
				sprite.update(delta);
				trans.y = SPEED * delta;
				
			}
			
		}
		
		if (input.isKeyDown(Input.KEY_A) && !showDiag /*|| input.isControllerLeft(Input.ANY_CONTROLLER)*/) {
			
			sprite = left;
			LEFT = true;
			
			if (!col.isBlocked(pos.x - delta * 0.2f, pos.y + WIDTH) && !col.isBlocked(pos.x - delta * 0.2f, pos.y) 
					&& !entCol.blockedSideLeft()) {
				
				sprite.update(delta);
				trans.x = -SPEED * delta;
				
			}
		}
		
		if (input.isKeyDown(Input.KEY_D ) && !showDiag /*|| input.isControllerRight(Input.ANY_CONTROLLER)*/) {
			
			sprite = right;
			RIGHT = true;
			
			if (!col.isBlocked(pos.x + WIDTH + delta * 0.2f, pos.y + HEIGHT) && !col.isBlocked(pos.x + WIDTH + delta * 0.2f, pos.y) 
					&& !entCol.blockedSideRight()) {
				
				sprite.update(delta);
				trans.x = SPEED * delta;
				
			}
		}
		if (input.isKeyDown(Input.KEY_E) && !ePressed /*|| input.isButton1Pressed(Input.ANY_CONTROLLER)*/) {
			
			ePressed = true;
			
			if (entCol.isBlocked()) {
				
				if (!npcs.get(npcs.indexOf(entCol.getCurrent())).getDialogue().getShown()) {
					showDiag = true;
				}
				
				if (npcs.get(npcs.indexOf(entCol.getCurrent())).getDialogue().getShown()) {
					showDiag = false;
				}
			}
		}
		if (input.isKeyDown(Input.KEY_GRAVE)) {
			
			System.out.println("X: " + pos.x);
			System.out.println("Y: " + pos.y);
		
		}
		
		/*if (input.isKeyDown(Input.KEY_Q) && !qPressed) {
			
			quak.play();
			qPressed = true;
		}*/
		
		if (trans.x != 0 && trans.y != 0) {
			trans.set(trans.x / 1.5f, trans.y / 1.5f);
		}
		
		if (pos.x + trans.x > 32 && pos.x + trans.x < (mapWidth-64)) {
			pos.x += trans.x;
		}
		
		if (pos.y + trans.y > 32 && pos.y + trans.y < (mapHeight-64)) {
			pos.y += trans.y;
		}
		if (input.isKeyDown(Input.KEY_ESCAPE)  && !escPressed/*|| input.isButtonPressed(7, 0)*/) {
			escPressed = true;
			Main.game.enterState(Main.PAUSE);
		}
		if (input.isKeyDown(Input.KEY_LSHIFT)) {
			sprinting = true;
			SPEED = .4f;
		}
		else if (!input.isKeyDown(Input.KEY_LSHIFT)) {
			sprinting = false;
			SPEED = .2f;
		}
		
		if (!input.isKeyDown(Input.KEY_E)) {
			ePressed = false;
		}
		if (!input.isKeyDown(Input.KEY_ESCAPE)) {
			escPressed = false;
		}
		if (!input.isKeyDown(Input.KEY_Q)) {
			qPressed = false;
		}
		
		if (!input.isKeyDown(Input.KEY_W)) {
			UP = false;
		}
		
		if (!input.isKeyDown(Input.KEY_S)) {
			DOWN = false;
		}
		
		if (!input.isKeyDown(Input.KEY_A)) {
			LEFT = false;
		}
		
		if (!input.isKeyDown(Input.KEY_D)) {
			RIGHT = false;
		}
		
		setBox(new Rectangle(pos.x, pos.y, WIDTH, HEIGHT));
		
			
	}
	
	public void init() throws SlickException {
		
		Image[] movementUp = new Image[totalSteps];
		Image[] movementDown = new Image[totalSteps];
		Image[] movementLeft = new Image[totalSteps];
		Image[] movementRight = new Image[totalSteps];
		int[] duration = new int[totalSteps];
		
		for (int i = 0; i < totalSteps; i++) {
			duration[i] = 50;
			movementUp[i] = sprites.getSprite(i, 0);
			movementDown[i] = sprites.getSprite(i, 0);
			movementRight[i] = sprites.getSprite(i, 0);
			movementLeft[i] = sprites.getSprite(totalSteps-i, 1);
		}

		up = new Animation(movementUp, duration, false);
		down = new Animation(movementDown, duration, false);
		left = new Animation(movementLeft, duration, false);
		right = new Animation(movementRight, duration, false);
		
		sprite = right;
		if (!entCol.isBlocked()) {
			currentNPC = null;
		}
	}
	
	public void render() throws SlickException {
		
		col.getMap().draw();
		sprite.draw(pos.x, pos.y);
		
		//Checks for player position in relation to map width and height to make sure the dialogue box renders properly
		if (showDiag) {
			//Center
			if (pos.x < mapWidth - 657 && pos.x > 657 && pos.y < mapHeight - 657 && pos.y - 657 > 0) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().showDialogueBox().draw(pos.x - 625, pos.y + 185, 1281, 192);
				for (int i = 0; i < npcs.get(npcs.indexOf(currentNPC)).getDialogue().getStrings().size(); i++) {
					dialogueFont.drawString(pos.x - 560, pos.y + 210 + 50 * i, npcs.get(npcs.indexOf(currentNPC)).getDialogue().currentLine(i));
				}
			}
			
			//Right
			else if (pos.x >= mapWidth - 657 && pos.y < mapHeight - 657 && pos.x - 657 > 0 && pos.y - 657 > 0) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().showDialogueBox().draw(mapWidth - 1280, pos.y + 185, 1281, 192);
				for (int i = 0; i < npcs.get(npcs.indexOf(currentNPC)).getDialogue().getStrings().size(); i++) {
					dialogueFont.drawString(mapWidth - 1230, pos.y + 210 + 50 * i, npcs.get(npcs.indexOf(currentNPC)).getDialogue().currentLine(i));
				}
			}
			//Bottom
			else if (pos.x < mapWidth - 657 && pos.y >= mapHeight - 657 && pos.x - 657 > 0 && pos.y - 657 > 0) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().showDialogueBox().draw(pos.x - 625, mapHeight - 192, 1281, 192);
				for (int i = 0; i < npcs.get(npcs.indexOf(currentNPC)).getDialogue().getStrings().size(); i++) {
					dialogueFont.drawString(pos.x - 560, mapHeight - 170 + 50 * i, npcs.get(npcs.indexOf(currentNPC)).getDialogue().currentLine(i));
				}
			}
			
			//Bottom Right
			else if (pos.x >= mapWidth - 657 && pos.y >= mapHeight - 657 &&pos.x - 657 > 0 && pos.y - 657 > 0) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().showDialogueBox().draw(mapWidth - 1280, mapHeight - 192, 1281, 192);
				for (int i = 0; i < npcs.get(npcs.indexOf(currentNPC)).getDialogue().getStrings().size(); i++) {
					dialogueFont.drawString(mapWidth - 1230, mapHeight - 170 + 50 * i, npcs.get(npcs.indexOf(currentNPC)).getDialogue().currentLine(i));
				}
			}
			//Left
			else if (pos.x < mapWidth - 657 && pos.y < mapHeight - 657 && pos.x - 657 <= 0 && pos.y - 657 > 0) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().showDialogueBox().draw(0, pos.y + 185, 1281, 192);
				for (int i = 0; i < npcs.get(npcs.indexOf(currentNPC)).getDialogue().getStrings().size(); i++) {
					dialogueFont.drawString(50, pos.y + 210 + 50 * i, npcs.get(npcs.indexOf(currentNPC)).getDialogue().currentLine(i));
				}
			}
			//Bottom Left
			else if (pos.x < mapWidth - 657 && pos.y >= mapHeight - 657 && pos.x - 657 <= 0 && pos.y - 657 > 0) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().showDialogueBox().draw(0, mapHeight - 192, 1281, 192);
				for (int i = 0; i < npcs.get(npcs.indexOf(currentNPC)).getDialogue().getStrings().size(); i++) {
					dialogueFont.drawString(50, mapHeight - 170 + 50 * i, npcs.get(npcs.indexOf(currentNPC)).getDialogue().currentLine(i));
				}
			}
			//Top
			else if (pos.x < mapWidth - 657 && pos.y < mapHeight - 657 && pos.x - 657 > 0 && pos.y - 657 <= 0) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().showDialogueBox().draw(pos.x - 625, 720 - 192, 1281, 192);
				for (int i = 0; i < npcs.get(npcs.indexOf(currentNPC)).getDialogue().getStrings().size(); i++) {
					dialogueFont.drawString(pos.x - 560, 720 - 170 + 50 * i, npcs.get(npcs.indexOf(currentNPC)).getDialogue().currentLine(i));
				}
			}
			//Top Left
			else if (pos.x < mapWidth - 657 && pos.y < mapHeight - 657 && pos.x - 657 <= 0 && pos.y - 657 <= 0) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().showDialogueBox().draw(0, 720 - 192, 1281, 192);
				for (int i = 0; i < npcs.get(npcs.indexOf(currentNPC)).getDialogue().getStrings().size(); i++) {
					dialogueFont.drawString(50, 720 - 170 + 50 * i, npcs.get(npcs.indexOf(currentNPC)).getDialogue().currentLine(i));
				}
			}
			//Top Right
			else if (pos.x >= mapWidth - 657 && pos.y < mapHeight - 657 && pos.x - 657 > 0 && pos.y - 657 <= 0) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().showDialogueBox().draw(mapWidth - 1280, 720 - 192, 1281, 192);
				for (int i = 0; i < npcs.get(npcs.indexOf(currentNPC)).getDialogue().getStrings().size(); i++) {
					dialogueFont.drawString(mapWidth - 1230, 720 - 170 + 50 * i, npcs.get(npcs.indexOf(currentNPC)).getDialogue().currentLine(i));
				}
			}
			
		}
		if (currentNPC != null) {
			if (!showDiag && npcs.get(npcs.indexOf(currentNPC)).getDialogue().getShown()) {
				npcs.get(npcs.indexOf(currentNPC)).getDialogue().hideDialogue();
			}
		}
		
	}
	
	//Getters and Setters
	public Vector2f getPos() {
		return pos;
	}
	
	public float getX() {
		return pos.x;
	}
	
	public float getY() {
		return pos.y;
	}
	
	public void setPos(Vector2f pos) {
		this.pos = pos;
	}
	
	public Rectangle getBox() {
		return box;
	}
	
	public void setBox(Rectangle box) {
		this.box = box;
	}
	
	public boolean rightEdgeDialogue() {
		if (pos.x >= mapWidth - 657) {
			return true;
		}
		return false;
	}
	
	public boolean bottomEdgeDialogue() {
		if (pos.y >= mapHeight - 657) {
			return true;
		}
		return false;
	}
	
	public boolean topEdgeDialogue() {
		if (pos.y - 657 <= 0) {
			return true;
		}
		return false;
	}
	
	public boolean leftEdgeDialogue() {
		if (pos.x - 657 < 0) {
			return true;
		}
		return false;
	}
	
	
}
