package org.kcsquared.marysue;

import java.io.IOException;

import org.kcsquared.marysue.collision.*;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

public class NPC {

	//Basic Variables
	private String name;
	private boolean walker;
	private Dialogue dialogue;
	
	//Animations
	private SpriteSheet sprites;
	private Animation sprite, up, down, left, right;
	private int totalSteps;

	//Position and Movement
	private Vector2f pos;
	private static final int SIZE = 32;
	private Collision col;
	boolean diag;
	private boolean countUp, countDown;
	private int counter;
	
	private Rectangle box;
	
	public NPC(float x, float y, int width, int height, int spacing, String name,boolean[] movement, int counter) throws SlickException {
		setPos(new Vector2f(x, y));
		box = new Rectangle(x, y, width, height);
		box.setBounds(x, y, width, height);
		
		dialogue = new Dialogue();
		this.counter = counter;
		
		
		this.countUp = movement[0];
		this.countDown = movement[1];
		this.walker = movement[2];
		this.diag = movement[3];
		this.name = name;
		this.sprites = new SpriteSheet("data/sprites/npc/" + this.name + "/" + this.name + ".png", width, height, spacing);
		
		totalSteps = sprites.getWidth() / (width + spacing);
		
		try {
			dialogue.getDialogueFromText(this.name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void init() {
		
		Image[] movementUp = new Image[this.totalSteps];
		Image[] movementDown = new Image[this.totalSteps];
		Image[] movementLeft = new Image[this.totalSteps];
		Image[] movementRight = new Image[this.totalSteps];
		int[] duration = new int[this.totalSteps];
		
		if (countDown) {
			counter = 100;
		}
		
		for (int i = 0; i < totalSteps; i++) {
			duration[i] = 75;
			movementUp[i] = sprites.getSprite(i, 0);
			movementDown[i] = sprites.getSprite(i, 0);
			movementRight[i] = sprites.getSprite(i, 0);
			movementLeft[i] = sprites.getSprite(totalSteps-i, 1);
		}

		up = new Animation(movementUp, duration, false);
		down = new Animation(movementDown, duration, false);
		left = new Animation(movementLeft, duration, false);
		right = new Animation(movementRight, duration, false);
		
		sprite = right;
	}
	
	public void render() {
		sprite.draw(getPos().x, getPos().y);
		
	}
	
	public Shape getBox() {
		return this.box;
	}
	
	public NPC getNPC() {
		return this;
	}
	
	public Dialogue getDialogue() {
		return this.dialogue;
	}

	public void update(GameContainer gc, int mapWidth, int mapHeight, int delta) throws SlickException {
		if (counter <= 200 && countUp) {
			sprite = right;
			counter++;
			getPos().x++;
			sprite.update(delta);
		}
		if (counter == 200) {
			countDown = true;
			countUp = false;
			counter--;
			getPos().x--;
		}
		if (counter >= 0 && countDown) {
			sprite = left;
			counter--;
			getPos().x--;
			sprite.update(delta);
		}
		if (counter == 0 && countDown) {
			countUp = true;
			countDown = false;
			counter++;
			getPos().x++;
		}
		box.setBounds(getPos().x, getPos().y, box.getWidth(), box.getHeight());
	}
	
	public boolean isWalker() {
		return this.walker;
	}

	public Vector2f getPos() {
		return pos;
	}

	public void setPos(Vector2f pos) {
		this.pos = pos;
	}
	
}
