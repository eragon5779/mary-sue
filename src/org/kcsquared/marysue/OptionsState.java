package org.kcsquared.marysue;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class OptionsState extends BasicGameState {

	private int stateID = -1;
	
	private int playerChoice = 0;
	private static final int NOCHOICES = 4;
	private static final int FPS = 0;
	private static final int VSYNC = 1;
	private static final int RESOLUTION = 2;
	private static final int BACK = 3;
	
	boolean controllerPressed = false;
	private boolean escPressed;
	private String[] playerOptions = new String[NOCHOICES];
	private Font font;
	private TrueTypeFont playerOptionsTTF;
	private Color notChosen = new Color(153,204,255);
	
	Image background;
	
	public OptionsState(int stateID) {
		this.stateID = stateID;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		
		font = new Font("Verdana", Font.BOLD, 40);
		playerOptionsTTF = new TrueTypeFont(font, true);
		font = new Font ("Verdana", Font.PLAIN, 20);
		background = new Image("data/backgrounds/EverfreeLowRez.png");
		
		playerOptions[0] = MarySue.getFPS() ? "FPS: ON" : "FPS: OFF";
		playerOptions[1] = MarySue.getVsync() ? "V-Sync: ON" : "V-Sync: OFF";
		playerOptions[2] = "Size: 1280x720";
		playerOptions[3] = "Back";
		
		
		
	}

	@Override
	public void render(GameContainer gb, StateBasedGame sgb, Graphics g) throws SlickException {
		
		background.draw(0,0,1280,720);
		renderPlayerOptions();
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sgb, int delta) throws SlickException {
		
		Input input = gc.getInput();
		if (input.isKeyPressed(Input.KEY_S) || (input.isControllerDown(Input.ANY_CONTROLLER) && !controllerPressed)) {
            controllerPressed = true;
            if (playerChoice == (NOCHOICES - 1)) {
                playerChoice = 0;
            } else {
                playerChoice++;
            }
        }
		if (input.isKeyPressed(Input.KEY_W) || (input.isControllerUp(Input.ANY_CONTROLLER) && !controllerPressed)) {
            controllerPressed = true;
            if (playerChoice == 0) {
                playerChoice = NOCHOICES - 1;
            } else {
                playerChoice--;
            }
        }
		if (input.isKeyPressed(Input.KEY_ESCAPE) && !escPressed) {
			escPressed = true;
			Main.game.enterState(Main.PLAY);
		}
		
		if (input.isKeyPressed(Input.KEY_ENTER) || (input.isButton1Pressed(Input.ANY_CONTROLLER) && !controllerPressed) ) {
			controllerPressed = true;
			switch (playerChoice) {
			case BACK : Main.game.enterState(Main.MENU); break;
			case FPS :  if (!MarySue.getFPS()) {MarySue.setFPS(true);}
			 			else {MarySue.setFPS(false);}
			 			break;
			case VSYNC : if (!MarySue.getVsync()) {MarySue.setVsync(true);}
						 else {MarySue.setVsync(false);}
						 break;
			}
			playerOptions[0] = MarySue.getFPS() ? "FPS: ON" : "FPS: OFF";
			playerOptions[1] = MarySue.getVsync() ? "V-Sync: ON" : "V-Sync: OFF";
		}
		
		if (!input.isControllerDown(Input.ANY_CONTROLLER) && !input.isControllerUp(Input.ANY_CONTROLLER) && !input.isButton1Pressed(Input.ANY_CONTROLLER)) {
			controllerPressed = false;
		}
		if (!input.isKeyPressed(Input.KEY_ESCAPE)) {
			escPressed = false;
		}
		
	}
	
	private void renderPlayerOptions() {
		
		
		
		for (int i = 0; i < NOCHOICES; i++) {
			if (playerChoice == i) {
				playerOptionsTTF.drawString(100, i * 50 + 200, playerOptions[i]);
			}
			else {
				playerOptionsTTF.drawString(100, i * 50 + 200, playerOptions[i], notChosen);
			}
		}
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return this.stateID;
	}

}
