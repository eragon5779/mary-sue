package org.kcsquared.marysue;

import java.util.ArrayList;

public class NPCCollision {
	
	private ArrayList<NPC> npcs;
	private final int SIZE = 32;
	
	private NPC currentNPC;
	private NPC npc;
	Player player;
	
	public NPCCollision(ArrayList<NPC> npcs, NPC npc) {
		this.npcs = npcs;
		this.npc = npc;
	}
	
	public boolean isBlocked() {
		for (int i = 0; i < npcs.size(); i++) {
			if (player.getBox().intersects(npcs.get(i).getBox())) {
				currentNPC = npcs.get(i);
				return true;
			}
		}
		return false;
	}
	
	public boolean blockedSideRight() {
		
		if (isBlocked()) {
			if (player.pos.x < npcs.get(npcs.indexOf(currentNPC)).getPos().x) {
				return true;
				
			}
		}
		return false;
	}
	public boolean blockedSideLeft() {
		if (isBlocked()) {
			if (player.pos.x > npcs.get(npcs.indexOf(currentNPC)).getPos().x) {
				return true;
			}
		}
		return false;
	}
	
	public boolean blockedSideTop() {
		if (isBlocked()) {
			if (player.pos.y < npcs.get(npcs.indexOf(currentNPC)).getPos().y) {
				return true;
			}
		}
		return false;
	}
	
	public boolean blockedSideBottom() {
		if (isBlocked()) {
			if (player.pos.y > npcs.get(npcs.indexOf(currentNPC)).getPos().y) {
				return true;
			}
		}
		return false;
	}
	
	public NPC getCurrent() {
		return this.currentNPC;
	}
	
}
