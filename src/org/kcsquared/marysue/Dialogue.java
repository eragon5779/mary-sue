package org.kcsquared.marysue;


import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import java.io.*;
import java.util.ArrayList;

public class Dialogue {

	private ArrayList<String> dialogue = new ArrayList<String>();
	private Image diagBox;
	private boolean isShown;
	
	
	
	public Dialogue() throws SlickException {
		
		
		diagBox = new Image("data/textures/textbox.png");
		
		
	}
	
	public String[] getDialogueFromText(String name) throws IOException {
		
		File dial = new File("data/dialogue/" + name + ".txt");
		
		FileReader fromFile = new FileReader(dial);
		BufferedReader bufFile = new BufferedReader(fromFile);
		String line;
		while ((line = bufFile.readLine()) != null) {
			dialogue.add(line);;
		}
		
		bufFile.close();
		
		return null;
	}
	
	public Image showDialogueBox() {
		isShown = true;
		return diagBox;
	}
	
	public void hideDialogue(){
		isShown = false;
	}
	
	public boolean getShown() {
		return this.isShown;
	}
	
	public String currentLine(int i) {
		return this.dialogue.get(i);
	}
	
	public ArrayList<String> getStrings() {
		return this.dialogue;
	}
	
}
