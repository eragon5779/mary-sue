package org.kcsquared.marysue;

import data.*;

import java.util.ArrayList;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.tiled.TileSet;
import org.newdawn.slick.tiled.TiledMap;

public class MarySue extends BasicGameState {
	
	//QUAK
	private Music quak;
	private int SHOULDQUAK = 0;
	
	//Real music
	static Music peace;
	
	//State info
	private int stateID;
	
	//NPCs
	static ArrayList<NPC> npcs = new ArrayList<NPC>();
	
	//Map Constants
	static int WIDTH = 1280;
	static int HEIGHT = 720;
	
	static boolean fullscreen = false;
	static boolean showFPS = Main.game.fps;
	static String title = "MarySue";
	static int fpsLimit = 2000; //VSync
	static boolean vsync = Main.vsync;
	
	//Variables
	private Map grassMap;
	protected static Player player;
	private static Duk duk98;
	private static Camera camera;
	int mapHeight, mapWidth;

	public MarySue(int stateID) {
		this.stateID = stateID;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		
		peace = new Music("data/music/PonyPeace.ogg");
		peace.setVolume(0);
		quak = new Music("data/music/quak.ogg");
		
		grassMap = new Map("data/maps/FirstLevel.tmx", "data/textures");
		
		mapWidth = grassMap.getWidth() * grassMap.getTileWidth(); //Map Size is tile size times tiles
		mapHeight = grassMap.getHeight() * grassMap.getTileHeight();
		
		//MAX NUMBER OF CHARACTERS IS 49 AND LINES IS 3
		
		boolean[] rarityBool = {true, false, true, false};
		boolean[] colgateBool = {false, false, false, false};
		boolean[] doctorBool = {false, false, false, false};
		boolean[] derpyBool = {true, false, true, true};
		boolean[] lyraBool = {false, false, false, false};
		boolean[] bonBool = {false, false, false, true};
		
		npcs.add(new NPC(1415,1645,58,52,6,"Rarity", rarityBool, 51));
		npcs.add(new NPC(1713, 1638, 49, 52, 15, "Colgate",colgateBool, 0));
		npcs.add(new NPC(1780, 1162, 45,54,18, "Doctor Whooves", doctorBool, 0));
		npcs.add(new NPC(2700, 1526, 53, 50, 11, "Derpy", derpyBool, 0));
		npcs.add(new NPC(500,1550,52,50,12,"Lyra",lyraBool, 0));
		npcs.add(new NPC(900,1175,50,49,14, "BonBon", bonBool, 0));
		
		player = new Player(813, 682, 53, 49, mapWidth, mapHeight, "FinishLine");
		setDuk98(new Duk(1145, 1572, 32, 32, "duk98", null));
		camera = new Camera(grassMap, mapWidth, mapHeight);
		
		player.init();
		getDuk98().init();
		for (int i = 0; i < npcs.size(); i++) {
			npcs.get(i).init();
		}
		
	}

	@Override
	public void enter(GameContainer gc, StateBasedGame sbg) {
		if (SHOULDQUAK == 0) {
			quak.play();
			SHOULDQUAK = 1;
		}
	}
	
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		camera.translate(g, player);
		
		grassMap.render(0, 0);
		
		for (int i = 0; i < npcs.size(); i++) {
			npcs.get(i).render();
		}
		getDuk98().render();
		player.render();
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		
		if (!quak.playing() && SHOULDQUAK == 1) {
			SHOULDQUAK = 2;
			peace.fade(3000, 5f, false);
			peace.loop();
		}
		
		player.update(gc, mapWidth, mapHeight, delta);
		getDuk98().update(gc, mapWidth, mapHeight, delta);
		for (int i = 0; i < npcs.size(); i++) {
			if (npcs.get(i).isWalker() && !npcs.get(i).getDialogue().getShown()) {
				npcs.get(i).update(gc, mapWidth, mapHeight, delta);
			}
		}
		
	}

	public ArrayList<NPC> getPlayers() {
		return npcs;
	}
	public static Player getPlayer() {
		return player;
	}
	
	@Override
	public int getID() {
		return stateID;
	}
	
	public Map getMap() {
		return grassMap;
	}
	
	public static boolean getVsync() {
		return vsync;
	}
	
	public static void setVsync(boolean vsync2) {
		Main.game.app.setVSync(vsync2);
		vsync = vsync2;
	}
	
	public static boolean getFPS() {
		return showFPS;
	}
	
	public static void setFPS(boolean fps) {
		Main.game.app.setShowFPS(fps);
		showFPS = fps;
	}
	
	public Camera getCamera() {
		return camera;
	}

	public static Duk getDuk98() {
		return duk98;
	}

	public static void setDuk98(Duk duk98) {
		MarySue.duk98 = duk98;
	}

}
