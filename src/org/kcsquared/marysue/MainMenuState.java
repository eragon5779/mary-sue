package org.kcsquared.marysue;

import java.awt.Font;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.Transition;

public class MainMenuState extends BasicGameState {

	private static int stateID = -1;
	
	private int playerChoice = 0;
	private static final int NOCHOICES = 5;
	private static final int START = 0;
	private static final int LOAD = 1;
	private static final int OPTIONS = 2;
	private static final int CREDITS = 3;
	private static final int QUIT = 4;
	
	private String[] playerOptions = new String[NOCHOICES];
	private boolean exit = false;
	boolean controllerPressed = false;
	private Font font;
	private TrueTypeFont playerOptionsTTF;
	private Color notChosen = new Color(153,204,255);
	Image background;
	
	private Music intro;
	private Music menuSelect;
	
	private Transition transition;
	
	public MainMenuState(int stateID) {
		MainMenuState.stateID = stateID;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

		font = new Font("Verdana", Font.BOLD, 40);
		playerOptionsTTF = new TrueTypeFont(font, true);
		font = new Font ("Verdana", Font.PLAIN, 20);
		background = new Image("data/backgrounds/EverfreeLowRez.png");
		intro = new Music("data/music/Intro.ogg");
		menuSelect = new Music("data/music/MenuSelect.ogg");
		transition = new CrossFadeTransition(2000);
		
		intro.play();
		
		playerOptions[0] = "Start";
		playerOptions[1] = "Load";
		playerOptions[2] = "Options";
		playerOptions[3] = "Credits";
		playerOptions[4] = "Quit";
		
		
		
	}

	@Override
	public void render(GameContainer gc, StateBasedGame abg, Graphics g) throws SlickException {
		
		background.draw(0,0, 1280, 720);
		renderPlayerOptions();
		if (exit) {
			gc.exit();
		}
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		
		Input input = gc.getInput();
		if (input.isKeyPressed(Input.KEY_S) || (input.isControllerDown(Input.ANY_CONTROLLER) && !controllerPressed)) {
            controllerPressed = true;
			if (playerChoice == (NOCHOICES - 1)) {
                playerChoice = 0;
            } else {
                playerChoice++;
            }
        }
        if (input.isKeyPressed(Input.KEY_W) || (input.isControllerUp(Input.ANY_CONTROLLER) && !controllerPressed)) {
            controllerPressed = true;
        	if (playerChoice == 0) {
                playerChoice = NOCHOICES - 1;
            } else {
                playerChoice--;
            }
        }
		
		if (input.isKeyPressed(Input.KEY_ENTER) || (input.isButton1Pressed(Input.ANY_CONTROLLER) && !controllerPressed)) {
			controllerPressed = true;
			switch (playerChoice) {
			case QUIT : exit = true; break;
			case START : Main.game.enterState(Main.PLAY, transition, null); intro.stop(); menuSelect.play(); break;
			case CREDITS : Main.game.enterState(Main.CREDITS); break;
			case OPTIONS : Main.game.enterState(Main.OPTIONS); break;
			}
		}
		
		if (!input.isControllerDown(Input.ANY_CONTROLLER) && !input.isControllerUp(Input.ANY_CONTROLLER) && !input.isButton1Pressed(Input.ANY_CONTROLLER)) {
			controllerPressed = false;
		}
		
	}
	
	private void renderPlayerOptions() {
		for (int i = 0; i < NOCHOICES; i++) {
			if (playerChoice == i) {
				playerOptionsTTF.drawString(100, i * 50 + 200, playerOptions[i]);
			}
			else {
				playerOptionsTTF.drawString(100, i * 50 + 200, playerOptions[i], notChosen);
			}
		}
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return MainMenuState.stateID;
	}

}
