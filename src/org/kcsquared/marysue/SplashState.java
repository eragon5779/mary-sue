package org.kcsquared.marysue;

import java.awt.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class SplashState extends BasicGameState {
	
	private String loading = "Loading.....";
	private TrueTypeFont loadingFont;
	private Font font;

	public SplashState(int ID) {
		
		font = new Font("Verdana", Font.BOLD, 40);
		loadingFont = new TrueTypeFont(font, true);
		
	}
	
	@Override
	public void init(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		
		
		
	}

	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics arg2) throws SlickException {
		
		loadingFont.drawString(525, 325, loading);
		
		
	}

	@Override
	public void update(GameContainer arg0, StateBasedGame arg1, int arg2) throws SlickException {
		// TODO Auto-generated method stub
		Main.game.loadStates();
		
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return Main.SPLASH;
	}

}
